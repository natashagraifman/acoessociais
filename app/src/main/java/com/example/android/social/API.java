package com.example.android.social;

import retrofit2.Retrofit;
import retrofit2.Converter;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.Call;
import com.google.gson.GsonBuilder;
import com.google.gson.Gson;
import com.google.gson.FieldNamingPolicy;

/**
 * Created by Natasha on 16/03/2018.
 */

public class API {
    private Service service;
    private static API instance; // que?

    public static API getInstance(){
        if(instance == null){
            instance = new API();
        }
        return instance;
    }

    private API(){
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl("https://dl.dropboxusercontent.com/")
                .addConverterFactory(GsonConverterFactory.create()) //pode??
                .build();
        //criar classe que vai implementar o Service
        this.service = retrofit.create(Service.class);
    }
    public Call<AcoesListEntity> getAcoes(){
        return service.pegaAcoes();
    }

}
