package com.example.android.social;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Natasha on 16/03/2018.
 */

public interface Service {
    @GET("s/f39meuzbspdlrhl/sociais.json")
    Call<AcoesListEntity> pegaAcoes();
}
