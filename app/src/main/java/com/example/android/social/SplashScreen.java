package com.example.android.social;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Natasha on 16/03/2018.
 */

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                openLista();
            }
        },2000);
    }

    public void openLista(){
        Intent intent = new Intent(SplashScreen.this,AcoesActivity.class);
        startActivity(intent);
        finish();
    }
    public void workOffline() {
        SharedPreferences preferences = getSharedPreferences("acoes",MODE_PRIVATE);
        String jsonAcoes = preferences.getString("json_acoes",null);
        openAcoes(jsonAcoes);
    }

    private void openAcoes(String jsonAcoes) {
        Intent abreOffline = new Intent(SplashScreen.this, AcoesActivity.class);
        abreOffline.putExtra("json_acoes",jsonAcoes);
        startActivity(abreOffline);
        finish();
    }
}
