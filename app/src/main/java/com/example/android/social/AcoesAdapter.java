package com.example.android.social;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

/**
 * Created by Natasha on 15/03/2018.
 */

public class AcoesAdapter extends RecyclerView.Adapter<AcoesAdapter.ViewHolder> {

    private List<AcoesEntity> acoesList;
    private Context context; // Que contexto é esse?
    OnRecyclerViewSelected onRecyclerViewSelected; // PRA QUE ISSO

    AcoesAdapter(List<AcoesEntity> acoesList, Context context){ // talvez tirar contexto
        this.acoesList = acoesList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.acoes_item_list,parent,false);
        return new ViewHolder(v);  // Se pede um retorno de ViewHolder porque eu estou retornando uma view?
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AcoesEntity acoesEntity = acoesList.get(position);
        holder.txAcoesTitle.setText(acoesEntity.getTitle());
        holder.txAcoesSite.setText(acoesEntity.getSite());
        Picasso.with(context)
                .load(acoesEntity.getCoverImageUrl())
                .centerCrop()
                .fit()
                .into(holder.imgBackground);
    }

    @Override
    public int getItemCount() {
        return acoesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.il_title)
        TextView txAcoesTitle;
        @BindView(R.id.il_site)
        TextView txAcoesSite;
        @BindView(R.id.il_background)
        ImageView imgBackground;


        public ViewHolder(View itemView) { // O que aconteceu aqui?
            super(itemView);
            ButterKnife.bind(this,itemView); // ?
        }

        @OnClick(R.id.container)
        void onItemClick(View view){
            if(onRecyclerViewSelected!= null)
                onRecyclerViewSelected.onClick(view,getAdapterPosition());
        }

        @OnLongClick(R.id.container)
        boolean onLongItemClick(View view) {
            if (onRecyclerViewSelected != null)
                onRecyclerViewSelected.onLongClick(view, getAdapterPosition());
            return true;
        }
    }

    public void setOnRecyclerViewSelected(OnRecyclerViewSelected onRecyclerViewSelected){
        this.onRecyclerViewSelected = onRecyclerViewSelected;
    }
}
