package com.example.android.social;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Natasha on 16/03/2018.
 */

public class AcoesDetailEntity {
    @SerializedName("nome")
    @Expose
    private String nome;
    @SerializedName("imagem")
    @Expose
    private String imagem;
    @SerializedName("descricao")
    @Expose
    private String descricao;
    @SerializedName("site")
    @Expose
    private String site;
    @SerializedName("id")
    @Expose
    private String id;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
