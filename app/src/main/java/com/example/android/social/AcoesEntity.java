package com.example.android.social;

/**
 * Created by Natasha on 15/03/2018.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class AcoesEntity {
    @SerializedName("nome")
    @Expose // PORQUE??
    private String title;
    @SerializedName("imagem")
    @Expose
    private String coverImageUrl;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("site")
    @Expose
    private String site;
    @SerializedName("descricao")
    @Expose
    private String descricao;

    AcoesEntity(String title, String site){
        this.title = title;
        this.site = site;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
