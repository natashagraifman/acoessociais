package com.example.android.social;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AcoesActivity extends AppCompatActivity {

    private static final String TAG = "vish";
    @BindView(R.id.acoes_rv)
    RecyclerView recyclerView;

    private AcoesListEntity acoesListEntity;

    SplashScreen splashScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acoes);
        ButterKnife.bind(this);

        String jsonAcoes = getIntent().getStringExtra("json_acoes");
        updateList(jsonAcoes);

    }

    public void updateList(String jsonAcoes){
        if(jsonAcoes!=null){
            acoesListEntity = new Gson().fromJson(jsonAcoes,AcoesListEntity.class);
            updateList(acoesListEntity.getAcoes());
        }
        else{
            updateListJSON();
        }

    }
    public void updateList(final List<AcoesEntity> acoesList){ // seta adapter
        AcoesAdapter acoesAdapter = new AcoesAdapter(acoesList, this);
        saveAcoesToJson();
        acoesAdapter.setOnRecyclerViewSelected(new OnRecyclerViewSelected() {
            @Override
            public void onClick(View view, int position) {
                Intent intent = new Intent(AcoesActivity.this, AcoesDetailActivity.class);

                intent.putExtra("acoes_id",acoesList.get(position).getId());
                intent.putExtra("nome",acoesList.get(position).getTitle());
                intent.putExtra("site",acoesList.get(position).getSite());
                intent.putExtra("descricao",acoesList.get(position).getDescricao());
                intent.putExtra("imagem",acoesList.get(position).getCoverImageUrl());
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        });

        recyclerView.setAdapter(acoesAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
    }

    public void updateListJSON(){
        final API api = API.getInstance();
        api.getAcoes().enqueue(new Callback<AcoesListEntity>() {
            @Override
            public void onResponse(Call<AcoesListEntity> call, Response<AcoesListEntity> response) {
                acoesListEntity = response.body();
                if(acoesListEntity!=null){
                    updateList(acoesListEntity.getAcoes());
                }
                else {
                    Toast.makeText(AcoesActivity.this, "erro", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AcoesListEntity> call, Throwable t) {
                Log.e(TAG,"Erro" + t.getMessage());
                splashScreen.workOffline();

            }
        });
    }
    // converter para String (json)
    public void saveAcoesToJson(){
        String jsonAcoes = new Gson().toJson(acoesListEntity);
        saveInSharedPreferences(jsonAcoes);
    }

    private void saveInSharedPreferences(String jsonAcoes) {
        SharedPreferences.Editor editor =
                getSharedPreferences("acoes",MODE_PRIVATE).edit();
        editor.putString("json_acoes",jsonAcoes);
        editor.apply();
        Toast.makeText(this, "Salvo", Toast.LENGTH_LONG).show();
    }


}
