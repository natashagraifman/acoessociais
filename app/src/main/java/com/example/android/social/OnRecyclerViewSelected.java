package com.example.android.social;

import android.view.View;

/**
 * Created by Natasha on 15/03/2018.
 */

public interface OnRecyclerViewSelected {
    void onClick(View view, int position);
    void onLongClick(View view, int position);
}
