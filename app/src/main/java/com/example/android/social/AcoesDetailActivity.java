package com.example.android.social;

import android.media.Image;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.util.List;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Natasha on 16/03/2018.
 */

public class AcoesDetailActivity extends AppCompatActivity {

    @BindView(R.id.detail_nome)
    TextView nome;
    @BindView(R.id.detail_description)
    TextView description;
    @BindView(R.id.detail_site)
    TextView site;
    @BindView(R.id.detail_image)
    ImageView imageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acoes_detail);
        ButterKnife.bind(this);
        showDetails();
    }

    private void showDetails() {
        String entityName = getIntent().getStringExtra("nome");
        String entitySite = getIntent().getStringExtra("site");
        String entityDescription = getIntent().getStringExtra("descricao");
        String entityImage = getIntent().getStringExtra("imagem");

        nome.setText(entityName);
        description.setText(entityDescription);
        site.setText(entitySite);
        Picasso.with(this)
                .load(entityImage)
                .centerCrop()
                .fit()
                .into(imageUrl);

    }
}
