package com.example.android.social;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Natasha on 16/03/2018.
 */

public class AcoesListEntity {
    @SerializedName("entidade_social_list")
    @Expose
    public List<AcoesEntity> acoesLista;
    public List<AcoesEntity> getAcoes(){
        return acoesLista;
    }
}
